import mongoose from 'mongoose';
import { Config } from '../types/config';

export const connectDb = async (config: Config): Promise<void> => {
  const { mongoConnectionUri } = config;

  /**
   *
   * 0 = disconnected
   * 1 = connected
   * 2 = connecting
   * 3 = disconnecting
   */
  if (mongoose.connection.readyState !== 1) {
    await mongoose.connect(mongoConnectionUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('Connected to MongoDB');
  } else {
    console.log('Already connected to MongoDB');
  }
};

export const disconnectDb = async (): Promise<void> => {
  for (const conn of mongoose.connections) {
    await conn.close();
  }
  console.log('Disconnected from MongoDB');
};
