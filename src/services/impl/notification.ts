import { ApisauceInstance } from 'apisauce';
import { NotificationService } from '../notification';
import { getConfig } from '../../config';

import { create } from 'apisauce';

export class NotificationServiceImpl implements NotificationService {
  private static api: ApisauceInstance;

  constructor() {
    NotificationServiceImpl.api = create({
      baseURL: 'https://slack.com/api',
      headers: {
        authorization: `Bearer ${getConfig().slackToken}`,
      },
    });
  }

  public async sendNotification(text: string, channel: string): Promise<void> {
    const config = getConfig();
    if (config.appEnv !== 'production' && !config.isRunningScript) return;
    const response = await NotificationServiceImpl.api.post<any>(
      `/chat.postMessage`,
      {
        text,
        channel,
      }
    );
    if (!response.ok) {
      console.log(
        `Field to send slack notification:`,
        JSON.stringify(response.data?.message, undefined, 2)
      );
    }
  }
}
