import DbEvent, { Event } from '../../models/event';
import { FromSchema } from 'json-schema-to-ts';
import { searchSchema } from '../../controllers/events/schema';
import { EventSearchService } from '../event';
import { PaginatedEvents } from '../../types/pagination';
import { DEFAULT_PAGE, DEFAULT_PAGE_SIZE } from '../../constants/search';

interface SearchEvents {
  filter: any;
  page: number;
  pageSize: number;
  sort?: Record<string, number>;
  allFields?: boolean;
}

export class EventSearchServiceImpl implements EventSearchService {
  public async search(
    params: FromSchema<typeof searchSchema>
  ): Promise<PaginatedEvents<Event>> {
    // default: search for ongoing events
    const { ongoing, upcoming, completed } = params;
    if (completed === 'true') {
      console.log('Search for completed events');
      return this.searchCompletedEvents(params);
    } else if (upcoming === 'true') {
      console.log('Search for upcoming events');
      return this.searchUpcomingEvents(params);
    } else {
      console.log('Search for ongoing events');
      return this.searchOngoingEvents(params);
    }
  }

  private async searchOngoingEvents(
    params: FromSchema<typeof searchSchema>
  ): Promise<PaginatedEvents<Event>> {
    const filter = this.getFilters(params);
    const startDate = params.startDate || new Date();

    filter.startDate = {
      $lte: startDate,
    };
    filter.endDate = {
      $gte: startDate,
    };

    console.log(filter);
    const { page, pageSize, ...restFilter } = filter;
    const result = await this.searchEvents({
      filter: restFilter,
      page,
      pageSize,
      allFields: params.allFields === 'true',
    });
    return result;
  }

  private async searchUpcomingEvents(
    params: FromSchema<typeof searchSchema>
  ): Promise<PaginatedEvents<Event>> {
    const filter = this.getFilters(params);
    const startDate = params.startDate || new Date();

    if (startDate) {
      filter.startDate = {
        $gt: startDate,
      };
    }
    console.log(filter);
    const { page, pageSize, ...restFilter } = filter;
    const result = await this.searchEvents({
      filter: restFilter,
      page,
      pageSize,
      allFields: params.allFields === 'true',
    });
    return result;
  }

  private async searchCompletedEvents(
    params: FromSchema<typeof searchSchema>
  ): Promise<PaginatedEvents<Event>> {
    const filter = this.getFilters(params);
    const endDate = params.endDate || new Date();

    filter.endDate = {
      $lt: endDate,
    };

    console.log(filter);
    const { page, pageSize, ...restFilter } = filter;
    const result = await this.searchEvents({
      filter: restFilter,
      page,
      pageSize,
      allFields: params.allFields === 'true',
      sort: { startDate: -1 },
    });
    return result;
  }

  /**
   * Extract filters from params
   * @param params
   * @returns // TODO: typing
   */
  private getFilters = (params: FromSchema<typeof searchSchema>): any => {
    const { searchTerm, source, type, isOnline, country, industry } =
      params || {};
    const page = params?.page || DEFAULT_PAGE;
    const pageSize = params?.pageSize || DEFAULT_PAGE_SIZE;

    const filter: any = {};
    filter.page = page;
    filter.pageSize = pageSize;
    if (searchTerm) {
      filter['$text'] = {
        $search: searchTerm,
      };
    }
    if (source) {
      filter.source = source;
    }
    if (type) {
      filter.type = type;
    }
    if (isOnline) {
      filter.isOnline = isOnline;
    }
    if (country) {
      filter.country = country;
    }
    if (industry) {
      filter.industries = {
        $in: [industry],
      };
    }
    return filter;
  };

  public async getDetail(id: string): Promise<Event> {
    const event = await DbEvent.findOne({ eventId: id });
    return event;
  }

  private async searchEvents(
    params: SearchEvents
  ): Promise<PaginatedEvents<Event>> {
    const { filter, page, pageSize, allFields = false, sort } = params;
    const defaultFields = {
      eventId: 1,
      title: 1,
      description: 1,
      location: 1,
      industries: 1,
      source: 1,
      type: 1,
      country: 1,
      startDate: 1,
      endDate: 1,
      // non-query fields
      utcOffset: 1,
      isOnline: 1,
      organizer: 1,
      image: 1,
      url: 1,
    };
    const options = allFields ? {} : defaultFields;
    const defaultSort = { startDate: 1 };
    const sortFilter = sort || defaultSort;

    const count = await DbEvent.count(filter);
    const numPages = Math.ceil(count / pageSize);
    const hasNextPage = page * pageSize < count;
    const events = await DbEvent.find(filter, options)
      .skip((page - 1) * pageSize)
      .limit(pageSize)
      .sort(sortFilter);
    return {
      count,
      numPages,
      hasNextPage,
      data: events,
    };
  }
}
