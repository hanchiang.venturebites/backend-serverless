export interface NotificationService {
  sendNotification(text: string, channel: string): Promise<void>;
}
