import { Event } from '../models/event';
import { FromSchema } from 'json-schema-to-ts';
import { searchSchema } from '../controllers/events/schema';
import { PaginatedEvents } from '../types/pagination';

export interface EventSearchService {
  /**
   * Search for events
   * @param params
   */
  search(
    params: FromSchema<typeof searchSchema>
  ): Promise<PaginatedEvents<Event>>;

  /**
   * Get event detail
   * @param id
   */
  getDetail(id: string): Promise<Event>;
}
