import { NotificationServiceImpl } from '../services/impl/notification';
import { TECH_ERRORS, TECH_WARNING } from '../constants/slack';

const notificationService = new NotificationServiceImpl();

/**
 * 1. If request is made from allowed origins: allow request to pass through
 * 2. Otherwise check for 'app-name' header
 * 3. If user agent is not allowed, check for 'app-name' header
 * @param headers
 */
export const authenticate = async (headers: Record<string, any>) => {
  const appName = headers['app-name'];
  const origin = headers.origin;
  const referer = headers.referer;
  const host = headers.Host;
  const userAgent = headers['User-Agent'];

  if (
    !isOriginAllowed(origin || referer || host) &&
    appName !== 'venturebites'
  ) {
    console.log('origin disallowed');
    // TODO: Sending to tech-warning isn't working
    notificationService.sendNotification(
      `Unauthorised origin detected. headers: ${JSON.stringify(
        headers,
        undefined,
        2
      )}`,
      TECH_WARNING
    );
    notificationService.sendNotification(
      `Unauthorised origin detected. headers: ${JSON.stringify(
        headers,
        undefined,
        2
      )}`,
      TECH_ERRORS
    );
    throw new Error('Unauthorised');
  }

  if (isUserAgentDisallowed(userAgent) && appName !== 'venturebites') {
    console.log('user agent disallowed');
    notificationService.sendNotification(
      `Unauthorised user agent detected. headers: ${JSON.stringify(
        headers,
        undefined,
        2
      )}`,
      TECH_WARNING
    );
    notificationService.sendNotification(
      `Unauthorised user agent detected. headers: ${JSON.stringify(
        headers,
        undefined,
        2
      )}`,
      TECH_ERRORS
    );
    throw new Error('Unauthorised');
  }
};

export const isOriginAllowed = (origin: string): boolean => {
  if (!origin) return true;
  const allowedOrigins = ['www.venturebites.co', 'venturebites.vercel.app'];
  for (const allowedOrigin of allowedOrigins) {
    if (origin.includes(allowedOrigin)) {
      return true;
    }
  }
  return false;
};
export const isUserAgentDisallowed = (userAgent: string) => {
  const disallowedUserAgents = ['postman', 'curl'];
  for (const disallowedUserAgent of disallowedUserAgents) {
    if (userAgent.toLowerCase().includes(disallowedUserAgent)) {
      return true;
    }
  }
  return false;
};
