import fs from 'fs';
import { Config } from '../types/config';
import path from 'path';

export const getConfig = (): Config => {
  const nodeEnv = process.env.NODE_ENV || 'development';
  const projectRoot = path.join(__dirname, '..', '..');
  if (nodeEnv === 'development') {
    console.log('Reading in development config...');
    require('dotenv').config({
      path: path.join(projectRoot, '.env.development.local'),
    });
  } else if (nodeEnv === 'production') {
    console.log('Reading in production config...');
    require('dotenv').config({
      path: path.join(projectRoot, '.env.production.local'),
    });
  } else {
    console.error(`NODE_ENV must be either development or production`);
    process.exit(1);
  }

  const config = {
    nodeEnv: process.env.NODE_ENV,
    appEnv: process.env.APP_ENV,
    isRunningScript: process.env.IS_RUNNING_SCRIPT === 'true',
    mongoConnectionUri: process.env.MONGO_CONNECTION_URI,
    slackToken: process.env.SLACK_TOKEN,
  };
  return config;
};

export const getConfigInScriptMode = (environment: string) => {
  const projectRoot = path.join(__dirname, '..', '..');
  if (environment === 'development') {
    return require('dotenv').parse(
      fs.readFileSync(path.join(projectRoot, '.env.development.local'))
    );
  } else if (environment === 'production') {
    return require('dotenv').parse(
      fs.readFileSync(path.join(projectRoot, '.env.production.local'))
    );
  } else {
    console.error(`environment must be either development or production`);
    process.exit(1);
  }
};
