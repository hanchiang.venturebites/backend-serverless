export interface PaginatedEvents<T> {
  count: number;
  numPages: number;
  hasNextPage: boolean;
  data: T[];
}
