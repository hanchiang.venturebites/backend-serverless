export interface Config {
  nodeEnv: string;
  appEnv: string;
  mongoConnectionUri: string;
  slackToken: string;
  isRunningScript: boolean;
}
