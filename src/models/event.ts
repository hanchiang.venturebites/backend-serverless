import { Schema, model, Document } from 'mongoose';
const { Types } = Schema;

interface IImage {
  small?: string;
  medium?: string;
  large?: string;
}

export interface Event {
  // Some sources(SGInnovate) don't have event id, so we use slug(should it be url instead?) as the id
  eventId?: string; // event id from source platform
  title: string;
  organizer: {
    id?: string;
    text: string;
    link?: string;
    logo?: string;
    description?: string;
  }; // e27, techinasia, individual person, etc
  source: string; // source of this event, aka where did we get this event from? e.g. e27, techinasia
  url: string;
  type?: string; // program, course, webinar, live, etc
  startDate: Date; // 2021-01-16T18:00:00.000+00:00
  endDate: Date; // 2021-01-17T19:00:00.000+00:00
  image?: IImage;
  isOnline: boolean;
  industries?: string[];
  description: string;
  googleCalendarLink?: string;
  // Ideally, we should store the IANA timezone string, but this seems almost impossible given
  // the random country/city names we get from sources
  // https://en.wikipedia.org/wiki/List_of_tz_database_time_zones, https://www.iana.org/time-zones
  timezone?: string;
  // some sources have dirty timezone strings, e.g. e27. Use this field to retrieve
  // unique values and handle them. To be deleted once there an elegant timezone
  // solution is implemented
  dirtyTimezone?: string;
  utcOffset?: number; // minutes
  country?: string;
  location?: string; // TODO: rename to city
  address?: string;
  registrationUrl?: string;
}

export interface DbEvent extends Event, Document {}

const eventSchema = new Schema(
  {
    title: { type: Types.String, required: true },
    eventId: { type: Types.String },
    organizer: {
      id: { type: Types.String },
      text: { type: Types.String, required: true },
      link: { type: Types.String },
      logo: { type: Types.String },
      description: { type: Types.String },
    },
    source: { type: Types.String, required: true },
    url: { type: Types.String, required: true },
    type: { type: Types.String },
    startDate: { type: Types.Date, required: true },
    endDate: { type: Types.Date, required: true },
    image: {
      small: { type: Types.String },
      medium: { type: Types.String },
      large: { type: Types.String },
    },
    isOnline: { type: Types.Boolean, required: true },
    industries: { type: [Types.String] },
    description: { type: Types.String, required: true },
    googleCalendarLink: { type: Types.String },
    registrationUrl: { type: Types.String },
    timezone: { type: Types.String },
    dirtyTimezone: { type: Types.String },
    utcOffset: { type: Types.Number },
    location: { type: Types.String },
    country: { type: Types.String },
    address: { type: Types.String },
  },
  { collection: 'Event', timestamps: true }
);

/**
 * Unique source and url for platforms that do not have event id. e.g. SG innovate
 */
eventSchema.index({ source: 1, url: 1, eventId: 1 }, { unique: true });
eventSchema.index(
  {
    title: 'text',
    description: 'text',
    location: 'text',
    industries: 'text',
    source: 1,
    type: 1,
    country: 1,
    startDate: 1,
    endDate: 1,
  },
  {
    weights: {
      title: 10,
      description: 5,
      location: 5,
      industries: 5,
    },
  }
);

const Event = model<DbEvent>('Event', eventSchema);
export default Event;
