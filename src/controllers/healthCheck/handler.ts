import { middyfy } from '@libs/lambda';
import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';

const healthCheck: ValidatedEventAPIGatewayProxyEvent<any> = async (
  event,
  context
) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return formatJSONResponse({
    response: {
      payload: 'API is up and running!',
    },
    headers: event.headers,
  });
};

export const healthCheckHandler = middyfy(healthCheck);
