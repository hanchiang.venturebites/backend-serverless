export const healthCheck = {
  handler: `${__dirname
    .split(process.cwd())[1]
    .substring(1)
    .replace(/\\/g, '/')}/handler.healthCheckHandler`,
  events: [
    {
      http: {
        method: 'get',
        path: '/healthz',
        cors: {
          origin: '*',
          headers: ['app-name', 'app-role'],
        },
        throttling: {
          maxRequestsPerSecond: 1,
          maxConcurrentRequests: 1,
        },
      },
    },
  ],
};
