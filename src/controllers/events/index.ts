export const searchEvents = {
  handler: `${__dirname
    .split(process.cwd())[1]
    .substring(1)
    .replace(/\\/g, '/')}/handler.searchEventHandler`,
  events: [
    {
      http: {
        method: 'get',
        path: 'events',
        request: {
          parameters: {
            querystrings: {
              searchTerm: false,
              source: false,
              type: false,
              isOnline: false,
              country: false,
              page: false,
              pageSize: false,
              startDate: false,
              ongoing: false,
              upcoming: false,
            },
          },
        },
        cors: {
          origin: '*',
          headers: ['app-name', 'app-role'],
        },
        throttling: {
          maxRequestsPerSecond: 50,
          maxConcurrentRequests: 25,
        },
      },
    },
  ],
};

export const getEventDetail = {
  handler: `${__dirname
    .split(process.cwd())[1]
    .substring(1)
    .replace(/\\/g, '/')}/handler.getEventDetailHandler`,
  events: [
    {
      http: {
        method: 'get',
        path: 'events/{id}',
        cors: {
          origin: '*',
          headers: ['app-name', 'app-role'],
        },
        throttling: {
          maxRequestsPerSecond: 50,
          maxConcurrentRequests: 30,
        },
      },
    },
  ],
};
