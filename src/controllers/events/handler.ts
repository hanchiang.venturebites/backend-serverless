import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { searchSchema, eventDetailSchema } from './schema';
import { getSchema } from './validator';
import { connectDb, disconnectDb } from '../../init/db';
import { EventSearchServiceImpl } from '../../services/impl/event';
import { NotificationServiceImpl } from '../../services/impl/notification';
import { TECH_ERRORS } from '../../constants/slack';
import { authenticate } from '../../utils/auth';
import { getConfig } from '../../config';

const searchEvents: ValidatedEventAPIGatewayProxyEvent<typeof searchSchema> =
  async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;
    try {
      await authenticate(event.headers);
    } catch (e) {
      return formatJSONResponse({
        response: {
          error: e.message,
        },
        statusCode: 401,
        headers: event.headers,
      });
    }

    const notificationService = new NotificationServiceImpl();
    const { queryStringParameters } = event;

    const querystring: any = {
      ...queryStringParameters,
    };
    if (queryStringParameters?.page) {
      querystring.page = parseInt(queryStringParameters.page);
    }
    if (queryStringParameters?.pageSize) {
      querystring.pageSize = parseInt(queryStringParameters.pageSize);
    }

    try {
      await getSchema(event.headers?.['app-role'] === 'admin').validate(
        querystring
      );
    } catch (e) {
      console.log(e);
      notificationService.sendNotification(`${e}\n${e.stack}`, TECH_ERRORS);
      return formatJSONResponse({
        response: {
          error: e.errors[0],
        },
        statusCode: 400,
        headers: event.headers,
      });
    }

    try {
      await connectDb(getConfig());
      const eventSearchService = new EventSearchServiceImpl();
      const events = await eventSearchService.search(querystring);
      return formatJSONResponse({
        response: {
          payload: events,
        },
        headers: event.headers,
      });
    } catch (e) {
      console.log(e);
      notificationService.sendNotification(`${e}\n${e.stack}`, TECH_ERRORS);
      return formatJSONResponse({
        response: {
          error: e.message,
        },
        statusCode: 500,
        headers: event.headers,
      });
    }
  };

const getEventDetail: ValidatedEventAPIGatewayProxyEvent<
  typeof eventDetailSchema
> = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  try {
    await authenticate(event.headers);
  } catch (e) {
    return formatJSONResponse({
      response: {
        error: e.message,
      },
      statusCode: 401,
      headers: event.headers,
    });
  }
  const notificationService = new NotificationServiceImpl();
  try {
    await connectDb(getConfig());
    const eventSearchService = new EventSearchServiceImpl();
    const { pathParameters } = event;
    const eventDetail = await eventSearchService.getDetail(pathParameters.id);
    return formatJSONResponse({
      response: {
        payload: eventDetail || {},
      },
      headers: event.headers,
    });
  } catch (e) {
    console.log(e);
    notificationService.sendNotification(`${e}\n${e.stack}`, TECH_ERRORS);
    return formatJSONResponse({
      response: {
        error: e.message,
      },
      statusCode: 500,
      headers: event.headers,
    });
  }
};

export const searchEventHandler = middyfy(searchEvents);
export const getEventDetailHandler = middyfy(getEventDetail);
