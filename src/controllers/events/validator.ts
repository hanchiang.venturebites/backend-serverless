import * as yup from 'yup';

export const getSchema = (isAdmin = false) =>
  yup
    .object({
      page: yup.number().min(1),
      pageSize: yup
        .number()
        .min(1)
        .max(isAdmin ? 1000 : 60),
      allFields: yup.string().oneOf(['true', 'false']),
      searchTerm: yup.string(),
      source: yup.string(),
      type: yup.string(),
      isOnline: yup.boolean(),
      country: yup.string(),
      startDate: yup.date(),
      ongoing: yup.string().oneOf(['true', 'false']),
      upcoming: yup.string().oneOf(['true', 'false']),
      completed: yup.string().oneOf(['true', 'false']),
      industry: yup.string(),
    })
    .nullable();
