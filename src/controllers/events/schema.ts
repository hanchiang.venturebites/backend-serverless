// TODO: Replace json schema with ts interface
export const searchSchema = {
  type: 'object',
  properties: {
    page: { type: 'number' },
    pageSize: { type: 'number' },
    allFields: { type: 'string' }, // 'true'
    searchTerm: { type: 'string' },
    source: { type: 'string' }, // e27, techinasia, etc
    type: { type: 'string' }, // conference, webinar, hackathon, etc
    isOnline: { type: 'boolean' }, // online|offline
    country: { type: 'string' },
    startDate: { type: 'string' }, // YYYY-MM-DD
    ongoing: { type: 'string' }, // 'true'
    upcoming: { type: 'string' }, // 'true'
    completed: { type: 'string' }, // 'true'
    industry: { type: 'string' },
  },
} as const;

export const eventDetailSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
  },
} as const;
