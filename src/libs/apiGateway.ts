import type {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Handler,
} from 'aws-lambda';
import type { FromSchema } from 'json-schema-to-ts';

type ValidatedAPIGatewayProxyEvent<S> = Omit<APIGatewayProxyEvent, 'body'> & {
  body: FromSchema<S>;
};
export type ValidatedEventAPIGatewayProxyEvent<S> = Handler<
  ValidatedAPIGatewayProxyEvent<S>,
  APIGatewayProxyResult
>;

interface Response {
  response: Record<string, unknown>;
  statusCode?: number;
  headers: Record<string, string>;
}

// TODO: cors for vercel: *-venturebites.vercel.app, e.g. https://frontend-6ws15lgdu-venturebites.vercel.app/
export const formatJSONResponse = (data: Response) => {
  const { response, statusCode = 200, headers } = data;
  const { origin } = headers;

  let allowedOrigin = 'https://www.venturebites.co';
  if (origin?.includes('venturebites.vercel.app')) {
    allowedOrigin = origin;
  }
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': allowedOrigin,
    },
    body: JSON.stringify(response),
  };
};
