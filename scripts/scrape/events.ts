import { performance } from 'perf_hooks';
import { connectDb, disconnectDb } from '../../src/init/db';
import DbEvent, { Event } from '../../src/models/event';
import { start as startE27 } from '../e27/event';
import { start as startTIA } from '../techInAsia/event';
import { start as startSGInnovate } from '../sginnovate/event';
import EditDistance from 'js-levenshtein';
import { getConfig } from '../../src/config';

// Entry point to scrape all events

/**
 * // TODO: Find a almost foolproof way
 * Deduplicate by calculating edit distance between 2 items
 * May produce false positive...
 * @param events
 */
const deduplicate = (events: Event[]): Event[] => {
  // Edit distance must be above this ratio in order to be deemed as not duplicate
  const minThreshold = 0.1;
  const result: Event[] = [];
  for (let i = 0; i < events.length - 1; i++) {
    const event1 = events[i];
    let isUnique = true;

    for (let j = i + 1; j < events.length; j++) {
      const event2 = events[j];
      const editDistance = EditDistance(event1.description, event2.description);

      const similarityEvent1 = editDistance / event1.description.length;
      const similarityEvent2 = editDistance / event2.description.length;
      console.log(
        `Edit distance: ${editDistance}, similarity event ${event1.eventId}: ${similarityEvent1}, similarity event ${event2.eventId}: ${similarityEvent2}`
      );

      if (similarityEvent1 < minThreshold || similarityEvent2 < minThreshold) {
        isUnique = false;
        console.log(
          `Event ${event1.eventId}, ${event1.title}, ${event1.description} is similar to event ${event2.eventId}, ${event2.title}, ${event2.description}`
        );
        break;
      }
    }
    if (isUnique) {
      result.push(event1);
    }
  }
  return result;
};

export const start = async () => {
  try {
    await connectDb(getConfig());
    const start = performance.now();
    const events = [];

    events.push(...(await startE27()));
    events.push(...(await startTIA()));
    events.push(...(await startSGInnovate()));

    console.log(`Retrieved ${events.length} events`);

    // TODO: Think of a good way to handle duplicate events across different sources
    // const deduplicated = deduplicate(result);
    // console.log(`Number of events after deduplicating content: ${deduplicated.length}`);

    console.log('Updating event types...');
    await updateEventTypes(events);

    // Save to DB
    console.log('Writing to database...');

    // TODO: Set ordered: true to debug errors
    const writeResult = await DbEvent.insertMany(events, { ordered: false });

    const end = performance.now();
    console.log(`Time taken for events: ${((end - start) / 1000).toFixed(2)}`);
  } catch (e) {
    console.log(e);
  } finally {
    await disconnectDb();
  }
};

const updateEventTypes = async (events) => {
  const types = ['webinar', 'workshop', 'demo day'];
  const otherType = 'Others';

  const eventWithOtherFilters = events.filter(
    (e) => !types.includes(e.type?.toLowerCase())
  );
  for (const e of eventWithOtherFilters) {
    e.type = otherType;
  }
};

// To run: ts-node scripts/scrape/events.ts
if (require.main === module) {
  start();
}
