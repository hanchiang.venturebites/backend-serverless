import fs from 'fs/promises';
import path from 'path';

import { connectDb, disconnectDb } from '../../../src/init/db';
import DbEvent, { Event } from '../../../src/models/event';
import { getConfigInScriptMode } from '../../../src/config';
import { Config } from '../../../src/types/config';

const start = async () => {
  try {
    const devConfig = getConfigInScriptMode('development');
    const parsedDevConfig = parseConfigAndSetEnv(devConfig);

    await connectDb(parsedDevConfig);
    const lastUpdatedAt = (await getLastUpdatedTimeStamp()) || new Date(0);
    const events = await DbEvent.find({ updatedAt: { $gt: lastUpdatedAt } });
    await disconnectDb();

    const prodConfig = getConfigInScriptMode('production');
    const parsedProdConfig = parseConfigAndSetEnv(prodConfig);

    await connectDb(parsedProdConfig);
    // TODO: Set ordered: true to debug errors
    await DbEvent.insertMany(events, { ordered: false });
    await disconnectDb();

    // await updateLastUpdatedAt();
  } catch (e) {
    console.log(e);
    await disconnectDb();
  }
};

const parseConfigAndSetEnv = (config: any): Config => {
  for (const key in config) {
    // console.log(key, config[key]);
    process.env[key] = config[key];
  }

  return {
    nodeEnv: process.env.NODE_ENV,
    appEnv: process.env.APP_ENV,
    isRunningScript: process.env.IS_RUNNING_SCRIPT === 'true',
    mongoConnectionUri: process.env.MONGO_CONNECTION_URI,
    slackToken: process.env.SLACK_TOKEN,
  };
};

const getLastUpdatedTimeStamp = async (): Promise<Date> => {
  try {
    const filepath = path.join(__dirname, 'lastUpdatedAt.txt');
    const lastUpdatedAt = await fs.readFile(filepath, { encoding: 'utf8' });
    return new Date(lastUpdatedAt);
  } catch (e) {
    return null;
  }
};

const updateLastUpdatedAt = async (): Promise<void> => {
  const filepath = path.join(__dirname, 'lastUpdatedAt.txt');
  await fs.writeFile(filepath, new Date().toISOString());
};

start();
