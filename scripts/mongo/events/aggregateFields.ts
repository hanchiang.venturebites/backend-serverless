import path from 'path';
import { getConfig } from '../../../src/config';
import { connectDb, disconnectDb } from '../../../src/init/db';
import Event from '../../../src/models/event';
import { writeFile } from '../../utils/file';

// Aggregate unique values of various fields in mongodb
// Copy result to /home/han/Documents/CODING-PROJECTS/VENTURE-BITES/frontend/data/eventAggregateFields.ts
const aggregateUniqueFields = async () => {
  try {
    await connectDb(getConfig());

    const typesAgg = await Event.aggregate([
      {
        $group: {
          _id: null,
          type: {
            $addToSet: '$type',
          },
        },
      },
    ]);
    const countriesAgg = await Event.aggregate([
      {
        $group: {
          _id: null,
          country: {
            $addToSet: '$country',
          },
        },
      },
    ]);
    const industriesAgg = await Event.aggregate([
      { $match: {} },
      {
        $unwind: {
          path: '$industries',
        },
      },
      {
        $group: {
          _id: null,
          industries: {
            $addToSet: '$industries',
          },
        },
      },
    ]);
    const locationsAgg = await Event.aggregate([
      {
        $group: {
          _id: null,
          location: {
            $addToSet: '$location',
          },
        },
      },
    ]);

    const sourcesAgg = await Event.aggregate([
      {
        $group: {
          _id: null,
          source: {
            $addToSet: '$source',
          },
        },
      },
    ]);

    const dirtyTimeZonesAgg = await Event.aggregate([
      {
        $group: {
          _id: null,
          dirtyTimezone: {
            $addToSet: '$dirtyTimezone',
          },
        },
      },
    ]);

    const types = typesAgg[0]?.type?.sort();
    const countries = countriesAgg[0].country.filter((c) => c).sort();
    const industries = industriesAgg[0].industries.sort();
    const locations = locationsAgg[0].location.filter((l) => l).sort();
    const sources = sourcesAgg[0].source.filter((s) => s).sort();
    const dirtyTimeZones = dirtyTimeZonesAgg[0].dirtyTimezone.filter((d) => d);

    const result = {
      types,
      countries,
      industries,
      locations,
      sources,
      dirtyTimeZones,
    };
    await writeFile(
      JSON.stringify(result, undefined, 2),
      path.join(__dirname, 'aggregateFields.json')
    );
  } catch (e) {
    console.log(e);
  } finally {
    await disconnectDb();
  }
};

// ts-node scripts/mongo/events/aggregateFields.ts
if (require.main === module) {
  aggregateUniqueFields();
}
