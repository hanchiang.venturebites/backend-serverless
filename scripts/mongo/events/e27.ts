import { connectDb, disconnectDb } from '../../../src/init/db';
import Event from '../../../src/models/event';
import { Source } from '../../../src/types/events';
import { getOffsetFromTimezoneString, parseDate } from '../../utils/e27';

/**
 * For e27 events. Store dirty time zone for processing into proper IANA timezone
 */
// export const setDirtyTimezone = async () => {
//   console.log('set dirty time zone for e27 events...');
//   await connectDb();

//   const events = await Event.find({ source: Source.E27 });
//   for (const event of events) {
//     if (!event.dirtyTimezone) {
//       event.dirtyTimezone = event.timezone;
//       await event.save();
//     }
//   }

//   await disconnectDb();
//   console.log('done!');
// };

/**
 * industry -> industries
 */
// export const updateIndustryName = async () => {
//   try {
//     await connectDb();
//     const events = await Event.find({});
//     for (const event of events) {
//       event.industries = event.industry;
//       await event.save();
//     }
//     await Event.updateMany({}, { $unset: { industry: "" } })
//   } catch (e) {
//     console.log(e);
//   } finally {
//     await disconnectDb();
//     console.log('done!');
//   }
// }

/**
 * parse dates into UTC dates, with utc offset
 */
// export const setUtcOffset = async () => {
//   const events = await Event.find({ source: Source.E27 });
//   for (const event of events) {
//     const offsetString = getOffsetFromTimezoneString(event.timezone);
//     const utcOffset = convertOffsetStringToInt(offsetString);
//     const startDate = parseDate(event.startDate, offsetString);
//     const endDate = parseDate(event.dateend, offsetString);
//     const dateAdded = parseDate(event.dateadded, offsetString);

//     event.utcOffset = utcOffset;
//     event.startDate = startDate.toISOString();
//     event.endDate = endDate.toISOString();
//     event.dateAdded = dateAdded.toISOString();
//   }
// }

// ts-node scripts/mongo/e27.ts
if (require.main === module) {
  // setDirtyTimezone();
  // setUtcOffset();
  // updateIndustryName();
}
