import querystring from 'querystring';
import puppeteer from 'puppeteer';
import dateFnsParse from 'date-fns/parse';
import dateFnsIsBefore from 'date-fns/isBefore';
import { SocksProxyAgent } from 'socks-proxy-agent';
import { performance } from 'perf_hooks';
import path from 'path';
import { sleep } from '../utils/sleep';
import { writeFile } from '../utils/file';
import { transformEvents } from '../utils/sginnovate';
import {
  SGInnovateEvent,
  SGInnovateEventDetail,
  SGInnovateEventFull,
  GetSGInnovateEvent,
} from '../types/sginnovate';
import { NotificationServiceImpl } from '../../src/services/impl/notification';
import { TECH_ERRORS, TECH_INFO } from '../../src/constants/slack';

const notificationService = new NotificationServiceImpl();
const EVENT_SEARCH_BASE_URL = 'https://www.sginnovate.com/event-search-result';

// tor
// const proxyOptions = new SocksProxyAgent('socks5://127.0.0.1:9050');

/**
 * Online event: https://www.sginnovate.com/events/next-step-deep-tech-asia
 * Private event: https://www.sginnovate.com/events/sginnovate-lo-hei-lunch-2020
 * Physical event: https://www.sginnovate.com/events/cybersecurity-%E2%80%93-you-cannot-live-without-it
 */

/**
 * Note page index starts at 0, events appear in ascending order of date
 * Best way to scrape is by year, i.e. field_event_date_value[value][year]=2021
 * e.g.
 * Minimalist: https://www.sginnovate.com/event-search-result?page=0
 * Year filter: https://www.sginnovate.com/event-search-result?combine=&field_event_categories_tid=All&field_event_date_value[value][year]=2021&field_event_date_value[value][month]=
 *
 * @param excludePastEvents
 */
const getEvents = async (
  excludePastEvents = false
): Promise<GetSGInnovateEvent> => {
  const BASE_URL = 'https://www.sginnovate.com';
  const browser = await puppeteer.launch({
    headless: true,
    timeout: 60000,
    executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium',
  });
  console.log(`Scraping events from SGInnovate...`);

  const yearFilter = 'field_event_date_value[value][year]';
  const queryObj = {
    [yearFilter]: 2021,
    page: 0,
  };

  const page = await browser.newPage();
  let pageUrl = getEventPageUrl(queryObj);
  const response = await page.goto(pageUrl, { timeout: 60000 });
  if (!response.ok) {
    await notificationService.sendNotification(
      `Error while retrieving events from SG Innovate url: ${pageUrl}, response: ${await response.text()}`,
      TECH_ERRORS
    );
    process.exit();
  }

  // get the last page number
  await page.waitForSelector('ul.pager');
  const lastPageNumber = await getLastPageNumber(page);
  if (lastPageNumber === undefined) {
    await notificationService.sendNotification(
      `Unable to find the last page number from ${pageUrl}`,
      TECH_ERRORS
    );
    process.exit();
  }
  console.log(`Last page number for ${pageUrl}: ${lastPageNumber}`);

  const result: GetSGInnovateEvent = {
    events: [],
    eventDetails: [],
  };
  /**
   * Start from last page, scrape towards the first page
   * Stop if events are earlier than current date
   */
  let currPage = lastPageNumber;
  let foundPastEvent = false;
  while (currPage >= 0) {
    pageUrl = getEventPageUrl({ ...queryObj, page: currPage });
    const response = await page.goto(pageUrl, { timeout: 60000 });
    if (!response.ok) {
      await notificationService.sendNotification(
        `Error while retrieving events from SG Innovate url: ${pageUrl}, response: ${await response.text()}`,
        TECH_ERRORS
      );
      process.exit();
    }

    // Get events
    await page.waitForSelector('ul.card-ul');
    const events = await getEventsFromPage(page);
    // parse date into dateFns date, in order to check whether event is in the past
    events.forEach((event) => {
      const now = new Date();
      // TODO: Should not parse here!
      // Should add utc offset(from event detail) into the date string and parse it
      event.startDate = dateFnsParse(
        event.startDateString,
        'EEEE, MMMM dd, yyyy',
        now
      );
      if (!foundPastEvent && dateFnsIsBefore(event.startDate, now)) {
        foundPastEvent = true;
      }
    });
    result.events.push(...events);

    // If utc offset length is more than 3, i.e. +10, alert to slack to see its format
    // such as +08:30, or whatever
    const eventDetails = await getEventDetailsForEvents(page, events);
    for (const eventDetail of eventDetails) {
      if (eventDetail.utcOffset?.length > 3) {
        await notificationService.sendNotification(
          `UTC offset with length longer than 3: ${
            eventDetail.utcOffset
          }, event: ${JSON.stringify(eventDetail, undefined, 2)}`,
          TECH_INFO
        );
      }
    }
    result.eventDetails.push(...eventDetails);
    currPage--;

    if (excludePastEvents && foundPastEvent) {
      console.log(
        'excludePastEvents and foundPastEvent are true, stopping scraping of SGInnovate events'
      );
      break;
    }
  }
  await browser.close();
  return result;
};

const getEventPageUrl = (queryObj: Record<string, any>): string => {
  return `${EVENT_SEARCH_BASE_URL}?${querystring.stringify(queryObj)}`;
};

const getLastPageNumber = async (
  page: puppeteer.Page
): Promise<number | undefined> => {
  return page.$eval<number | undefined>(
    'li.pager-last.last',
    (liTag): number | undefined => {
      const aTag = liTag.querySelector('a');
      // e.g. https://www.sginnovate.com/event-search-result?combine=&field_event_categories_tid=All&field_event_date_value%5Bvalue%5D%5Byear%5D=2021&field_event_date_value%5Bvalue%5D%5Bmonth%5D=&page=3
      const href = aTag.getAttribute('href');
      const regex = /&?page=(\d)/;
      const match = href.match(regex);
      if (match) {
        return parseInt(match[1]);
      }
      return undefined;
    }
  );
};

const getEventsFromPage = async (
  page: puppeteer.Page
): Promise<SGInnovateEvent[]> => {
  return page.$$eval<SGInnovateEvent[]>('li', (liTags): SGInnovateEvent[] => {
    const events = [];
    for (const liTag of liTags) {
      const imgTag = liTag.querySelector('figure > img.card-img-top');
      if (!imgTag) {
        continue;
      }
      const aTag = liTag.querySelector('a');
      const href = aTag.getAttribute('href');
      const imgSrc = imgTag.getAttribute('src');

      const title = liTag.querySelector('.card-body h4.card-title').textContent;
      const paragraphs = liTag?.querySelectorAll('.card-body > p');
      const description = paragraphs[1].textContent;
      const topics = Array.from(paragraphs[3]?.querySelectorAll('a') || [])
        .map((aTag: any) => aTag.textContent)
        .reduce((result: string[], curr: string) => {
          const splitCurr = curr.split(/\//).map((str) => str.trim());
          return [...result, ...splitCurr];
        }, []);

      const footer = liTag.querySelector('div.card-footer');
      const eventDateParagraph = footer.querySelector('p.event-date');
      const eventLocationParagraph = footer.querySelector('p.event-location');
      const eventDate = eventDateParagraph.querySelector('strong').textContent;

      const buttonTag = liTag.querySelector('div.card-btn-hld');
      const linkTag = buttonTag.querySelector('a');
      let registrationUrl;
      /**
       * 'ENDED' if event has ended
       * 'register interest' or 'indicate interest' if it is a new course?
       * 'register' if it is an event?
       */
      if (linkTag.textContent.toLowerCase() === 'register') {
        registrationUrl = linkTag.getAttribute('href');
      }
      // 'Online Event', 'Private Event', or address of event
      // TODO: country is a pain in the ass to scrape...
      const addressOrOnline = eventLocationParagraph.textContent;
      const isOnline = addressOrOnline.toLowerCase().includes('online');

      const event: any = {
        image: imgSrc,
        url: `https://www.sginnovate.com${href}`,
        title,
        description,
        topics,
        isOnline,
        startDateString: eventDate,
      };
      if (registrationUrl) event.registrationUrl = registrationUrl;
      if (!isOnline) event.address = addressOrOnline;
      events.push(event);
    }
    return events;
  });
};

const getEventDetailsForEvents = async (
  page: puppeteer.Page,
  events: SGInnovateEvent[]
): Promise<SGInnovateEventDetail[]> => {
  const eventDetails: SGInnovateEventDetail[] = [];

  for (const event of events) {
    const { url } = event;
    await page.goto(url);
    await page.waitForSelector('section.events-details');

    const eventDetail = await page.$eval<SGInnovateEventDetail>(
      'section.events-details .row > div:last-child',
      (section): SGInnovateEventDetail => {
        const subcaption = section.querySelector('.subcaption')?.textContent;
        const organiserRegex = /Presented\sby\s(.*)$/;
        const match = subcaption.match(organiserRegex);
        let organiser;
        if (match) {
          organiser = match[1];
        }

        const eventDetail: any = {
          organiser,
        };
        /**
         * All the event detail in string... Need to extract time...
         * Time: 10:00am to 11:00am (Singapore Time / UTC +8)
         * Time: 11:00am - 12:00pm (Singapore Time / UTC +8)
         * Regex to the rescue!
         * TODO: Wow, this section of the event detail is fucked. The structure varies quite a bit...
         * 1. Fucked up without time: https://www.sginnovate.com/events/leaders%E2%80%99-dialogue-future-enterprise-technologies
         * 2. p > strong: https://www.sginnovate.com/events/promoting-technological-cooperation-between-poland-and-singapore
         * 3. div > strong: https://www.sginnovate.com/events/powering-future-industry-growth-deep-tech
         * 4. p > span: https://www.sginnovate.com/events/next-step-deep-tech-asia
         * 5. p: https://www.sginnovate.com/events/asean-deep-tech-economy-resilient-and-sustainable-future
         */
        const firstParagraph = section.querySelector('p');
        const content4 = firstParagraph?.querySelector('span')?.textContent;
        const content2 = firstParagraph?.querySelector('strong')?.textContent;
        const content3 = section
          ?.querySelector('div')
          ?.querySelector('strong')?.textContent;
        const content5 = firstParagraph?.textContent;
        const content = content4 || content2 || content3 || content5;

        /**
         * group 1 = fromTime, e.g. 11:00
         * group 2 = am/pm
         * group 3 = '-' or 'to'
         * group 4 = toTime, e.g. 12:00
         * group 5 = am/pm
         * group 6 = UTC
         * group 7 = utc offset, e.g. +8, maybe +8:30?
         */
        const timeRegex =
          /Time:\s*(\d+:\d+)(\w+)\s*(to|-)\s*(\d+:\d+)(\w+)\s*\(.*\/\s*(\w+)\s*(\+.*)\)/;
        const timeMatch = content?.match(timeRegex);
        if (timeMatch) {
          const [_, fromTime, fromAmPm, __, toTime, toAmPm, utc, utcOffset] =
            timeMatch;
          eventDetail.startTime = `${fromTime}${fromAmPm.toUpperCase()}`;
          eventDetail.endTime = `${toTime}${toAmPm.toUpperCase()}`;
          eventDetail.utcOffset = utcOffset;
        }
        return eventDetail;
      }
    );
    eventDetails.push(eventDetail);
  }
  return eventDetails;
};

/**
 * Main part
 */
export const start = async (): Promise<any> => {
  const start = performance.now();

  // 1. Retrieve events
  // events.push(...(await getEvents()));
  const { events, eventDetails } = await getEvents();
  console.log(
    `Found ${events.length} events, ${eventDetails.length} event details`
  );
  await writeFile(
    JSON.stringify(events.slice(0), undefined, 2),
    path.join(__dirname, 'events.js')
  );
  await writeFile(
    JSON.stringify(eventDetails.slice(0), undefined, 2),
    path.join(__dirname, 'eventDetails.js')
  );

  // 2. transform and validate events
  const transformed = await transformEvents(events, eventDetails);
  console.log(`There are ${transformed.length} events after transformation`);
  await writeFile(
    JSON.stringify(transformed, undefined, 2),
    path.join(__dirname, 'transformedEvents.js')
  );

  const end = performance.now();
  // 229 seconds for 47 events...
  console.log(
    `Time taken for TechInAsia events: ${((end - start) / 1000).toFixed(2)}`
  );
  return transformed;
};

// To run: ts-node scripts/sginnovate/event.ts
if (require.main === module) {
  start();
}
