#! /bin/bash

# to run: scripts/jobs/events/events.sh

dirname=$(dirname "$0")

scrapeEventsToLocalDb () {
  echo "Scraping events to local database..."
  ts-node "../scrape/events.ts"
  echo "Completed scraping events to local database!"
}

dumpLocalDb () {
  echo "Dumping local database using mongodump..."
  mongodump --uri="mongodb://root:root@localhost:27017/venturebites?authSource=admin" --out="../../mongodump"
  echo "Completed dumping local database"
}

exportLocalDb () {
  echo "Exporting local database using mongoexport..."
  mongoexport --collection="Event" --uri="mongodb://root:root@localhost:27017/venturebites?authSource=admin" --out="../../mongoexport/Event.json"
  echo "Completed exporting local database"
}

ingestEventsToProdDb () {
  echo "Ingesting events to production database..."
  ts-node "../mongo/events/ingestToProd.ts"
  echo "Completed ingesting events to production database!"
}

aggregateEventFields () {
  echo "Aggregating unique event fields..."
  ts-node "../mongo/events/aggregateFields.ts"
  echo "Completed unique event fields..."
}

scrapeEventsToLocalDb
dumpLocalDb
exportLocalDb
ingestEventsToProdDb
aggregateEventFields