#! /bin/bash

# to run: scripts/jobs/index.sh


# start docker on MacOS
start_time=$(date +%s)

# open /Applications/Docker.app/Contents/MacOS/Docker\ Desktop.app
open -a Docker
echo "Waiting for docker to start..."
sleep 20
echo "Docker started!"

dirname=$(dirname "$0")
cd $dirname

docker-compose up -d

bash events/index.sh

today=$(date +"%Y-%m-%d %H:%M:%S %z")
echo $today >> "updatedAt.txt"

docker-compose down

# stop docker on MacOS
# Killall Docker
# pkill -SIGHUP -f /Applications/Docker.app 'docker serve' 

end_time=$(date +%s)

time_elapsed=$((end_time - start_time))
echo "Time elapsed: $time_elapsed seconds"