export const eventFields = [
  'id',
  'date_gmt',
  'title',
  'excerpt',
  'link',
  'type',
  'live_items',
  'author',
  'featured_image',
  'categories',
  'tags',
];

export const nameMapping = {
  id: 'eventId',
  link: 'url',
  excerpt: 'description',
};
