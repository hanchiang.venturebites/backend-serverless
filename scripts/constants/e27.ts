export const eventFields = [
  'name',
  'link',
  'type',
  'datestart',
  'dateend',
  'leadimage',
  'isonline',
  'country',
];

export const eventDetailFields = [
  'id',
  'markets',
  'event_synopsis',
  'event_reglink',
  'timezone',
  'is_owner',
  'dateadded',
  'location',
  'organizer',
];

export const nameMapping = {
  // event
  name: 'title',
  link: 'url',
  datestart: 'startDate',
  dateend: 'endDate',
  leadimage: 'image',
  isonline: 'isOnline',
  // event detail
  id: 'eventId',
  markets: 'industries',
  event_synopsis: 'description',
  event_reglink: 'registrationUrl',
  is_owner: 'isOwner',
  dateadded: 'dateAdded',
};
