import { create } from 'apisauce';
import { TIAEventDetailResponse, TIAEventDetail } from '../types/techInAsia';
import { checkAndHandleApiError } from '../utils/apiError';

const api = create({
  baseURL: 'https://www.techinasia.com/wp-json/techinasia/2.0',
  headers: {
    'user-agent':
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
    accept:
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
  },
});

// Event detail: e.g. https://www.techinasia.com/events/workshop-successfully-recruit-retain-top-tech-talent
// e.g. https://www.techinasia.com/wp-json/techinasia/2.0/posts/workshop-successfully-recruit-retain-top-tech-talent
export const getEventDetail = async (slug: string): Promise<TIAEventDetail> => {
  const res = await api.get<TIAEventDetailResponse>(`/posts/${slug}`);
  checkAndHandleApiError(res);
  return res?.data?.posts?.[0];
};
