import { create } from 'apisauce';
import { SocksProxyAgent } from 'socks-proxy-agent';
import { performance } from 'perf_hooks';
import path from 'path';
import querystring from 'querystring';
import { TIAEventResponse, TIAEvent } from '../types/techInAsia';
import { checkAndHandleApiError } from '../utils/apiError';
import { sleep } from '../utils/sleep';
import { writeFile } from '../utils/file';
import { transformEvents } from '../utils/techInAsia';
import { Event } from '../../src/models/event';
// import { getEventDetail } from './eventDetail';

// tor
// const proxyOptions = new SocksProxyAgent('socks5://127.0.0.1:9050');
// https://spys.one/en/socks-proxy-list/
// const proxyOptions = new SocksProxyAgent('socks5://95.216.207.20:15276');

const api = create({
  baseURL: 'https://www.techinasia.com/wp-json/techinasia/2.0',
  headers: {
    // 'user-agent':
    //   'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
    // accept:
    //   'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    // 'accept-encoding': 'gzip, deflate, br',
    // 'accept-language': 'en-US,en;q=0.9',
    // 'cache-control': 'max-age=0',
    // 'cookie': 'PHPSESSID=6vt9ch59eeva8ipdulloanbk1t; reset_cookie=1; _hjid=523db23c-15e6-47b9-aad6-26fb02b8731a; drift_aid=a2a03419-c64c-43e4-94cb-7cb46951a2ae; driftt_aid=a2a03419-c64c-43e4-94cb-7cb46951a2ae; _hjMinimizedPolls=608440%2C647548%2C607879; site_user_id=ab0dea30287a6160562348e422fb37a3; __cfduid=d453792eac41e6ddc85678db0f06d28921612886715; ci_session=a%3A5%3A%7Bs%3A10%3A%22session_id%22%3Bs%3A32%3A%22708c29c0ec5f7d0596855c33019fe41c%22%3Bs%3A10%3A%22ip_address%22%3Bs%3A15%3A%22162.158.165.252%22%3Bs%3A10%3A%22user_agent%22%3Bs%3A104%3A%22Mozilla%2F5.0+%28X11%3B+Linux+x86_64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F87.0.4280.88+Safari%2F537.36%22%3Bs%3A13%3A%22last_activity%22%3Bi%3A1612886715%3Bs%3A9%3A%22user_data%22%3Bs%3A0%3A%22%22%3B%7De29d67c5608526ea243e164c6fc07d5d; site_user_name=yap-chiang; _gid=GA1.2.1944997347.1612886733; __cf_bm=89c8602f20c63457bd26aa57b59717c601d22932-1613089856-1800-AWMQTnAjEufCcmyRyB8VWO1765jbKHVlBFl9xtV4AqUV4AbXLTll9dTfrpk+jk1xx4L/bZ1CuN/lmLgubkqTkVc2197XxylT/wkR2Oei8X8jrNKiB4V4IcRrUwZbJAaAdA==; _ga_D1580XMZB4=GS1.1.1613089860.13.0.1613089860.60; _ga=GA1.1.547961105.1610028316; drift_campaign_refresh=d977c007-771d-4382-a02a-015df01fc7d8',
    // dnt: '1',
    // 'sec-ch-ua': 'Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99',
    // 'sec-ch-ua-mobile': '?0',
    // 'sec-fetch-mode': 'document',
    // 'sec-fetch-site': 'none',
    // 'sec-fetch-user': '?1',
    // 'upgrade-insecure-requests': 1
  },
  // httpsAgent: proxyOptions
});

const PAGE_SIZE = 30; // Hard limit of techinasia

/**
 * params:
 * page: integer(optional)
 * per_page: integer(optional)
 */

// TODO: Notify to slack if there are errors
// e.g. https://www.techinasia.com/wp-json/techinasia/2.0/categories/events/posts?page=1&per_page=30
const getEvents = async (excludePastEvents = true): Promise<TIAEvent[]> => {
  const query = {
    page: 1,
    per_page: PAGE_SIZE,
  };
  console.log('Getting TIA events...');
  const result = await paginate(
    '/categories/events/posts',
    query,
    excludePastEvents
  );
  return result;
};

interface Query {
  page: number;
  [key: string]: any;
}

export const paginate = async (
  url: string,
  query: Query,
  excludePastEvents: boolean
) => {
  const result: TIAEvent[] = [];
  let hasExecutedOnce = false;

  while (true) {
    console.log(`Getting page ${query.page}...`);
    const res = await retry<TIAEventResponse>(
      `${url}?${querystring.stringify(query)}`
    );
    const { posts: events, total, total_pages } = res;
    result.push(...events);

    if (!hasExecutedOnce) {
      console.log(`There are ${total} events, ${total_pages} pages`);
      hasExecutedOnce = true;
    }
    if (excludePastEvents && !hasMoreEventsInFuture(events)) {
      console.log('No more events in the future');
      break;
    }
    if (query.page >= total_pages) {
      break;
    }
    query.page++;
    await sleep(0.5, 3);
  }
  return result;
};

/**
 * TIA return events in reverse chronological order
 * @param events
 * @returns
 */
const hasMoreEventsInFuture = (events: TIAEvent[]): boolean => {
  const lastEvent = events[events.length - 1];
  return lastEvent.date_gmt > new Date().toISOString();
};

const retry = async <T>(url: string, timesToTry: number = 3): Promise<T> => {
  let retryCount = 0;
  while (retryCount < timesToTry) {
    // apisauce
    console.log(`Getting data using apisauce...`);
    const res = await api.get<T>(url);
    try {
      checkAndHandleApiError(res);
      return res.data;
    } catch (e) {
      await sleep(2 + retryCount * 2, 3 + retryCount * 2);
      console.log(`Failed to get data using apisauce. Retrying...`);
      retryCount++;
    }
  }
  throw new Error(`Failed to get data after trying a maximum of ${timesToTry}`);
};

// const getEventDetails = async (
//   eventIds: string[]
// ): Promise<TIAEventDetail[]> => {
//   console.log(`Getting event details for ${eventIds.length} events...`);
//   const promises = eventIds.map((eventId) => getEventDetail(eventId));
//   const eventDetails = await Promise.all(promises);
//   return eventDetails;
// };

export const deduplicateEvents = (
  events: TIAEvent[]
): { eventIds: string[]; dedupEvents: TIAEvent[] } => {
  const eventIds = new Set<string>();
  const dedupEvents: TIAEvent[] = [];

  for (const event of events) {
    const { id } = event;
    if (!eventIds.has(id)) {
      eventIds.add(id);
      dedupEvents.push(event);
    }
  }
  return { eventIds: Array.from(eventIds), dedupEvents };
};

/**
 * Main part
 */
export const start = async (): Promise<Event[]> => {
  const start = performance.now();

  // 1. Retrieve events
  const events: TIAEvent[] = [];

  events.push(...(await getEvents()));

  console.log(`Retrieved ${events.length} events from TechInAsia`);
  await writeFile(
    JSON.stringify(events.slice(0, 3), undefined, 2),
    path.join(__dirname, 'events.js')
  );

  // 2. Deduplicate events by event id
  const { dedupEvents } = deduplicateEvents(events);
  console.log(`Number of events after deduplication: ${dedupEvents.length}`);

  // 3. transform result
  const transformed = transformEvents(dedupEvents);
  await writeFile(
    JSON.stringify(transformed, undefined, 2),
    path.join(__dirname, 'transformedEvents.js')
  );

  console.log(`Number of events after transformation: ${transformed.length}`);
  const end = performance.now();
  // 5.97 seconds for 30 events...
  console.log(
    `Time taken for TechInAsia events: ${((end - start) / 1000).toFixed(2)}`
  );

  return transformed;
};

// To run: ts-node scripts/techInAsia/event.ts
if (require.main === module) {
  start();
}
