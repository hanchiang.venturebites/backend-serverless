interface BaseResponse {
  total: number;
  per_page: number;
  current_page: number;
  total_pages: number;
}

export interface TIAEvent {
  id: string;
  date_gmt: string; // Start date? Not really wtf . e.g. 2021-03-02T09:18:49
  // TODO: No end date? assume end date is the same as start date?
  modified_gmt: string; // Useless? e.g. 2021-03-08T09:34:15
  title: string;
  slug: string;
  status: string; // e.g. publish. WTF is this?
  type: string; // e.g. live, post. Filter for 'live' types only
  link: string; // event url
  content: string; // html
  vsitems: any; // No idea what this is
  live_items?: TIALiveItem; // only for type = 'live'
  excerpt: string; // description
  // Similar to 'organizer' in models/event
  author: {
    id: string;
    first_name: string;
    last_name: string;
    display_name: string;
    description: string;
    avatar_url: string;
    author_url: string;
  };
  featured_image: {
    source: string;
    attachment_meta: {
      width: number;
      height: number;
      sizes: {
        medium: { width: number; height: number; url: string };
        large: { width: number; height: number; url: string };
        thumbnail: { width: number; height: number; url: string };
      };
    };
    cloudinary_meta: any;
  };
  seo: {
    title: string;
    description: string;
    image: string;
    canonical_link: any;
  };
  categories: {
    id: string;
    name: string; // Conference
    slug: string; // conference
  }[];
  tags: {
    id: string;
    name: string; // Infographic
    slug: string; // infographic
  }[];
  companies: any;
  is_sponsored: boolean;
  sponsor: {
    logo?: string;
    name?: string;
    link?: string;
  };
  is_partnership: boolean;
  show_ads: boolean;
  is_subscriber_exclusive: boolean;
  is_paywalled: boolean;
  read_time: number;
}

export interface TIALiveItem {
  venue: string; // Virtual
  city: string; // Singapore
  virtual: string; // Virtual,
  price: string; // "18.90",
  video: string; // iframe video
  cutoff_date: string; // YYYY-MM-DD
  cutoff_time: string; // 3:00pm
  start_date: string; // YYYY-MM-DD
  start_time: string; // 4:00pm
  end_date: string; // YYYY-MM-DD
  end_time: string; // 5:00pm
  event_venue: string; // Virtual
  event_price: string; // 18.90
  event_video: string; // iframe video
  event_cutoff_date: string; // YYYY-MM-DD
  event_cutoff_time: string; // 3:00pm
  event_cutoff_dt_utc: string; // 20210224070000
  event_cutoff_dt_utc_tz: string; // 20210224T070000Z
  event_cutoff_dt_sgt: string; // 20210224150000,
  event_start_date: string; // YYYY-MM-DD
  event_start_time: string; // 4:00pm
  event_end_date: string; // YYYY-MM-DD
  event_end_time: string; // 5:00pm
  event_start_dt_sgt: string; // 20210224 160000
  event_end_dt_sgt: string; // 20210224 170000,
  event_start_dt_utc_tz: string; // 20210224T080000Z,
  event_end_dt_utc_tz: string; // 20210224T090000Z,
  event_end_dt_utc_YmdHis: string; // 20210224090000,
  event_duration_date_sgt: string; // "Wednesday, 24 February 2021",
  event_duration_time_sgt: string; // 4:00PM to 5:00PM (GMT+8),
  event_duration_date_start_sgt: string; // Wednesday, 24 February 2021,
  event_duration_date_end_sgt: string; // Wednesday, 24 February 2021,
  event_duration_time_start_sgt: string; // 4:00PM
  event_duration_time_end_sgt: string; // 5:00PM
  event_link: string;
  gcal_start_date_utc: string; // 20210224
  gcal_start_time_utc: string; // 080000
  gcal_end_date_utc: string; // 20210224
  gcal_end_time_utc: string; // 090000
  gcal_link: string;
}

export interface TIAEventResponse extends BaseResponse {
  posts: TIAEvent[];
}

export type TIAEventDetail = TIAEvent;

export type TIAEventDetailResponse = TIAEventResponse;

/**
 * Combination of TIAEvent and TIAEventDetal
 */

export type EventFull = TIAEvent;

export interface ParsedDate {
  startDate: string;
  endDate: string;
}
