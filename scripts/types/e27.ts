interface BaseResponse {
  elapsed_time: string; // seconds in float
}

export interface E27Event {
  name: string;
  organizer: string; // e.g. e27
  link: string; // event url
  type: string; // e.g. Webinar
  datestart: string; // YYYY-MM-DD HH:mm:ss, e.g. 2021-02-03 13:00:00
  dateend: string; // YYYY-MM-DD HH:mm:ss
  // https://e27.co/img/events/15711/ba6b5b0b-0020-41dd-a4a9-448ed2119fae-lead-image-small.png
  // image url contains event id, i.e. 15711. But not all events' images follow this format...
  leadimage?: {
    small?: string;
    medium?: string;
    large?: string;
  };
  isonline: string; // 1 or 0
  country?: string;
}

export interface EventResponse extends BaseResponse {
  data: {
    events: E27Event[];
    count: number;
    pages: number;
  };
}

export interface E27EventDetail {
  id: string;
  event_name: string; // "name" in E27Event
  approved: string; // 1 or 0
  'posted-event': string; // What's this?
  type: string; // e.g. Webinar. Same as "type" in E27Event
  markets: string[]; // Agritech, Artificial intelligence, etc
  event_synopsis: string;
  event_reglink: string;
  timezone: string; // (GMT+08:00) Kuala Lumpur, Singapore
  isonline: string; // 1 or 0
  country?: string;
  // person who posted the event
  site_user: {
    name: string;
    url: string;
    logo: string;
  };
  is_owner: boolean;
  leadimage: string; // e.g. 2021-02-03 13:00:00
  datestart: string;
  dateend: string;
  dateadded: string;
  location: string; // city?
  organizer: {
    id: string;
    text: string;
    link: string;
    logo: string;
    description: string;
  };
}

export interface EventDetailResponse extends BaseResponse {
  data: E27EventDetail;
}

/**
 * Combination of E27Event and E27EventDetail
 */
export interface EventFull {
  /**
   * From E27Event
   */
  name: string;
  link: string;
  type: string;
  datestart: string; // YYYY-MM-DD HH:mm:ss
  dateend: string; // YYYY-MM-DD HH:mm:ss
  // https://e27.co/img/events/15711/ba6b5b0b-0020-41dd-a4a9-448ed2119fae-lead-image-small.png
  // image url contains event id, i.e. 15711
  leadimage?: {
    small?: string;
    medium?: string;
    large?: string;
  };
  isonline: string; // 1 or 0
  country?: string;
  /**
   * From E2EventDetail
   */
  id: string;
  markets: string[];
  event_synopsis: string;
  event_reglink: string;
  timezone: string;
  is_owner: boolean;
  dateadded: string;
  location: string;
  organizer: {
    id: string;
    text: string;
    link: string;
    logo: string;
    description: string;
  };
}

export interface ParsedDate {
  utcOffset: number;
  startDate: string;
  endDate: string;
  dateAdded: string;
}
