export interface SGInnovateEvent {
  // TODO: no event type?
  image: string;
  url: string;
  title: string;
  description: string;
  topics: string[];
  registrationUrl?: string;
  address?: string;
  isOnline: boolean;
  startDateString: string; // parse into startDate. To be removed
  // TODO: Remove startDate
  startDate: string | Date; // EEEE, MMMM dd, YYYY, e.g. Wednesday, January 13, 2021
  endDate?: string;
}

export interface SGInnovateEventDetail {
  organiser: string;
  startTime: string; // 11:00AM
  endTime: string; // 12:00PM
  utcOffset: string; // +8, +12, maybe +8:30?
}

/**
 * Combination of SGInnovateEvent and SGInnovateEventDetail
 */
export interface SGInnovateEventFull
  extends SGInnovateEvent,
    SGInnovateEventDetail {}

export interface GetSGInnovateEvent {
  events: SGInnovateEvent[];
  eventDetails: SGInnovateEventDetail[];
}
