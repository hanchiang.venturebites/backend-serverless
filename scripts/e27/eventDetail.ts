import { create } from 'apisauce';
import { EventDetailResponse, E27EventDetail } from '../types/e27';
import { checkAndHandleApiError } from '../utils/apiError';

const api = create({
  baseURL: 'https://e27.co/api',
  headers: {
    'user-agent':
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
    accept:
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
  },
});

// Event detail: e.g. https://e27.co/event/singapore-smart-nation-api-colab-api-lab-api-nights-api-hack-day/
// e.g. https://e27.co/api/event/get/view/?id=15711
export const getEventDetail = async (id: string): Promise<E27EventDetail> => {
  const res = await api.get<EventDetailResponse>(`/event/get/view/?id=${id}`);
  checkAndHandleApiError(res);
  return res.data.data;
};
