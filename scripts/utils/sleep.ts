/**
 * Returns random number in seconds
 * If min and max is not give, number will be in the range [0, 1)
 * If min is given and max is not given, number will be in the range [min, min+1)
 * If both are given, number will be in the range [min, max)
 * @param min
 * @param max
 */
export const getRandomNumber = (min?: number, max?: number): number => {
  if (!min && !max) {
    return Math.random();
  }
  if (min && !max) {
    return Math.random() + min;
  }
  // Scale by (max-min)
  // Add min
  const scaleFactor = max - min;
  return Math.random() * scaleFactor + min;
};

export const sleep = (min?: number, max?: number) => {
  const ms = getRandomNumber(min || 0.5, max || 2) * 1000;
  console.log(`Sleeping for ${(ms / 1000).toFixed(2)} seconds...`);
  return new Promise((resolve) => setTimeout(resolve, ms));
};
