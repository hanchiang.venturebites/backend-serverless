import { TIAEvent } from '../types/techInAsia';
import { eventFields, nameMapping } from '../constants/techInAsia';
import { Source } from '../../src/types/events';
import { Event } from '../../src/models/event';
import _ from 'lodash';

/**
 * Trasform event data by adding event details
 * @param events
 */
export const transformEvents = (events: TIAEvent[]): Event[] => {
  console.log('Transforming events...');
  const result = [];

  // Add event details to events
  for (let i = 0; i < events.length; i++) {
    const isOnline = events[i].type === 'live';
    // Filter for type 'live' only
    if (!isOnline) {
      continue;
    }

    // pick fields
    const combinedEvent: any = {
      ..._.pick(events[i], eventFields),
      isOnline,
      source: Source.TIA,
    };

    // rename field names
    for (const [origKey, destKey] of Object.entries(nameMapping)) {
      combinedEvent[destKey] = combinedEvent[origKey];
      delete combinedEvent[origKey];
    }

    // event type, categories, tags
    combinedEvent.type = combinedEvent?.categories?.[0]?.name;
    delete combinedEvent.categories;
    delete combinedEvent.tags;

    // Transform organizer
    combinedEvent.organizer = {
      text: combinedEvent.author.display_name,
      link: combinedEvent.author.author_url,
    };
    delete combinedEvent.author;

    // Transform image
    const { medium, large, thumbnail } =
      combinedEvent.featured_image?.attachment_meta?.sizes || {};
    if (medium || large || thumbnail) {
      combinedEvent.image = {
        small: thumbnail?.url,
        medium: medium?.url,
        large: large?.url,
      };
    }
    delete combinedEvent.featured_image;

    // live_items
    // Start date and end date
    const { live_items } = combinedEvent;
    const startDate = live_items?.event_start_dt_utc_tz;
    const endDate = live_items?.event_end_dt_utc_tz;
    const startDateISO = parseDate(startDate);
    const endDateISO = parseDate(endDate);

    combinedEvent.startDate = startDateISO;
    combinedEvent.endDate = endDateISO;
    delete combinedEvent.event_start_dt_utc_tz;
    delete combinedEvent.event_end_dt_utc_tz;

    delete combinedEvent['live_items'];
    delete combinedEvent.date_gmt;

    // country
    combinedEvent.country = live_items?.city;

    // Google calendar link
    combinedEvent.googleCalendarLink = live_items?.gcal_link;

    result.push(combinedEvent as Event);
  }
  return result;
};

/**
 *
 * @param date e.g. 20210224T080000Z
 */
const parseDate = (date: string): string => {
  const year = date.substring(0, 4);
  const month = date.substring(4, 6);
  const day = date.substring(6, 8);
  const hour = date.substring(9, 11);
  const minute = date.substring(11, 13);
  const second = date.substring(13, 15);

  return `${year}-${month}-${day}T${hour}:${minute}:${second}Z`;
};
