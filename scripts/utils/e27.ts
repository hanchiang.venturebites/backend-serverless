import { E27Event, E27EventDetail, ParsedDate } from '../types/e27';
import { eventDetailFields, eventFields, nameMapping } from '../constants/e27';
import { Source } from '../../src/types/events';
import { Event } from '../../src/models/event';
import _ from 'lodash';
import dateFnsParse from 'date-fns/parse';

/**
 * Trasform event data by adding event details
 * @param events
 */
export const transformEvents = (
  events: E27Event[],
  eventDetails: E27EventDetail[]
): Event[] => {
  console.log('Transforming events...');
  const result = [];

  if (events.length !== eventDetails.length) {
    console.log(
      `Length of events and event details are different. Events: ${events.length}, event details: ${eventDetails.length}`
    );
  }
  // Add event details to events
  for (let i = 0; i < events.length; i++) {
    // pick fields
    const combinedEvent: any = {
      ..._.pick(events[i], eventFields),
      ..._.pick(eventDetails[i], eventDetailFields),
    };
    // rename field names
    for (const [origKey, destKey] of Object.entries(nameMapping)) {
      combinedEvent[destKey] = combinedEvent[origKey];
      delete combinedEvent[origKey];
    }
    // Add fields, transform fields
    combinedEvent.source = Source.E27;
    combinedEvent.isOnline = combinedEvent.isOnline === '1';
    combinedEvent.eventId = eventDetails[i].id;

    // TODO: Build a mapping of country -> timezone.
    // Omit events that don't have mapping. Manually update the mappings and add the event
    // timezone!
    // Ignore events that don't have timezone string
    if (!eventDetails[i].timezone) {
      console.log(`${events[i].name} does not have timezone. Skipping...`);
      combinedEvent.startDate = eventDetails[i].datestart;
      combinedEvent.endDate = eventDetails[i].dateend;
      combinedEvent.dateAdded = eventDetails[i].dateadded;
      continue;
    }
    combinedEvent.dirtyTimezone = combinedEvent.timezone;
    delete combinedEvent.timezone;

    const { utcOffset, startDate, endDate, dateAdded } = parseDates(
      eventDetails[i]
    );
    combinedEvent.utcOffset = utcOffset;
    combinedEvent.startDate = startDate;
    combinedEvent.endDate = endDate;
    combinedEvent.dateAdded = dateAdded;

    result.push(combinedEvent as Event);
  }
  return result;
};

/**
 * Parse dates from timezone, return dates in UTC and utc offset
 * @param eventDetail
 */
export const parseDates = (eventDetail: E27EventDetail): ParsedDate => {
  const offsetString = getOffsetFromTimezoneString(eventDetail.timezone);
  const utcOffset = convertOffsetStringToInt(offsetString);
  const startDate = parseDate(eventDetail.datestart, offsetString);
  const endDate = parseDate(eventDetail.dateend, offsetString);
  const dateAdded = parseDate(eventDetail.dateadded, offsetString);

  return {
    utcOffset,
    startDate: startDate.toISOString(),
    endDate: endDate.toISOString(),
    dateAdded: dateAdded.toISOString(),
  };
};

/**
 * (GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London
 * (GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi
 * (GMT-10:00) Hawaii
 * (GMT-08:00) Pacific Time (US & Canada)
 * Compulsory: Get GMT offset, e.g. (GMT+00:00) - \(\w+(\+|-)\d+:\d+\)
 * Optional: Get Country in bracket, e.g. (US & Canada) - \s?\(.*$
 * @param timezone
 */
export const splitDirtyTimezoneIntoCities = (timezone: string): string[] => {
  if (timezone.length === 0) return [];

  const offsetRegex = /\(\w+(\+|-)\d+:\d+\)/;
  const countryBracketRegex = /\s?\(.*$/;
  const removedOffset = timezone.replace(offsetRegex, '').trim();
  const removedCountryBracket = removedOffset
    .replace(countryBracketRegex, '')
    .trim();
  return removedCountryBracket.split(',').map((s: string) => s.trim());
};

/**
 * Return offset in minutes
 * @param offset +08:00/-08:00
 */
export const convertOffsetStringToInt = (offset: string): number => {
  if (offset.length !== 6) {
    console.log(`Offset is not in the correct format of +00:00: ${offset}`);
    return 0;
  }
  const sign = offset[0];
  const offsetWithoutSign = offset.substring(1);
  const [hour, minute] = offsetWithoutSign
    .split(':')
    .map((num: string) => parseInt(num));
  const result = hour * 60 + minute;
  return sign === '+' ? result : -1 * result;
};

/**
 * e.g. (GMT+08:00) Kuala Lumpur, Singapore. return +08:00
 * @param tzString
 */
export const getOffsetFromTimezoneString = (
  tzString: string
): string | undefined => {
  const regex = /((\+|\-)\d+:\d+)/;
  const match = tzString.match(regex);
  if (!match) {
    console.log(`Unable to find offset from ${tzString}`);
    return;
  }
  return match[1];
};

/**
 * @param date 2021-01-30 10:30:50
 * @param offset +08:00/-08:00
 */
export const parseDate = (date: string, offset: string): Date => {
  return dateFnsParse(
    `${date} ${offset}`,
    'yyyy-MM-dd HH:mm:ss XXX',
    new Date()
  );
};

/**
 * e.g. https://e27.co/img/events/15665/aec4cc42-394d-4514-a953-94d6fc972b19-lead-image-small.png
 * @param url
 */
export const getEventIdFromImage = (url: string): string | undefined => {
  const regex = /img\/events\/(\d+)\//;
  const match = url.match(regex);
  if (!match) {
    console.log(`Unable to find event id from image: ${url}`);
    return;
  }
  return match[1];
};
