import dateFnsParse from 'date-fns/parse';
import { SGInnovateEvent, SGInnovateEventDetail } from '../types/sginnovate';
import { Source } from '../../src/types/events';
import { Event } from '../../src/models/event';
import _ from 'lodash';
import { NotificationServiceImpl } from '../../src/services/impl/notification';
import { TECH_WARNING } from '../../src/constants/slack';
import { nameMapping } from '../constants/sginnovate';

const notificationService = new NotificationServiceImpl();

/**
 * Trasform event data by adding event details
 * @param events
 */
export const transformEvents = async (
  events: SGInnovateEvent[],
  eventDetails: SGInnovateEventDetail[]
): Promise<Event[]> => {
  console.log('Transforming events...');
  const result = [];

  if (events.length !== eventDetails.length) {
    await notificationService.sendNotification(
      `Length of events and event details are different. Events: ${events.length}, event details: ${eventDetails.length}`,
      TECH_WARNING
    );
  }
  // Add event details to events
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const eventDetail = eventDetails[i];
    const eventCombined: any = {
      ...event,
      ...eventDetail,
      source: Source.SGInnovate,
    };
    if (
      !eventDetail.startTime &&
      !eventDetail.endTime &&
      !eventDetail.utcOffset
    ) {
      continue;
    }
    delete eventCombined.startDateString;

    eventCombined.image = {
      large: eventCombined.image,
    };

    const startDate = eventCombined.startDate;
    eventCombined.startDate = formatDateString(
      startDate as Date,
      eventCombined.startTime,
      eventCombined.utcOffset
    );
    eventCombined.endDate = formatDateString(
      startDate as Date,
      eventCombined.endTime,
      eventCombined.utcOffset
    );

    eventCombined.organizer = {
      text: eventCombined.organiser,
    };
    delete eventCombined.organiser;

    eventCombined.utcOffset = utcOffsetInMinutes(eventCombined.utcOffset);

    delete eventCombined.startTime;
    delete eventCombined.endTime;

    // pick fields
    // const combinedEvent: any = {
    //   ..._.pick(events[i], eventFields),
    //   ..._.pick(eventDetails[i], eventDetailFields),
    // };

    // rename field names
    for (const [origKey, destKey] of Object.entries(nameMapping)) {
      eventCombined[destKey] = eventCombined[origKey];
      delete eventCombined[origKey];
    }
    // Add fields, transform fields

    result.push(eventCombined);
  }
  return result;
};

/**
 * Get YYYY-MM-DD from date
 * @param date 2021-01-12T16:00:00.000Z
 * @param time 11:00AM
 * @param utcOffset +8, +830?
 * @returns
 */
export const formatDateString = (
  date: Date,
  time: string,
  utcOffset: string
): string => {
  const dateString = date.toISOString();
  const year = date.getFullYear().toString();
  let month = (date.getMonth() + 1).toString();
  if (month.length === 1) {
    month = `0${month}`;
  }
  let day = date.getDate().toString();
  if (day.length === 1) {
    day = `0${day}`;
  }
  // +8 -> +800, -8 -> -800
  let offset = utcOffset;
  if (offset.length <= 2) {
    offset = `${offset}00`;
  }
  // +830 -> +0830, +800 -> +0800
  if (offset.length === 4) {
    offset = `${offset[0]}0${offset.substring(1)}`;
  }

  const formattedDateString = `${year}-${month}-${day} ${time}${offset}`;
  const formattedDate = dateFnsParse(
    formattedDateString,
    'yyyy-MM-dd hh:mmaxx',
    new Date()
  );
  return formattedDate.toISOString();
};

export const utcOffsetInMinutes = (utcOffset: string): number => {
  const sign = utcOffset[0];
  // 8, 10, 830?
  const rest = utcOffset.substring(1);
  let hour = parseInt(rest[0]);
  let minutes = 0;
  if (rest.length > 2) {
    minutes = parseInt(rest.substring(1));
  } else if (rest.length === 2) {
    hour = parseInt(rest.substring(0, 2));
  }
  return hour * 60 + minutes;
};
