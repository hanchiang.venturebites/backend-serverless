import fs from 'fs';
import util from 'util';

const writeFileAsync = util.promisify(fs.writeFile.bind(fs));

/**
 *
 * @param data
 * @param path
 * @param mode w = write, a = append, r = read
 */
export const writeFile = async (data: any, path: string, mode = 'w') => {
  await writeFileAsync(path, data, { encoding: 'utf-8', flag: mode });
};
