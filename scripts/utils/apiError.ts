import { ApiResponse } from 'apisauce';

// TODO: rename
export const checkAndHandleApiError = <T>(res: ApiResponse<T>): void => {
  if (!res.ok) {
    console.log(`${res.status}, ${res.problem}`);
    console.log(res.originalError);
    throw new Error('Error occurred from apisauce');
  }
};
