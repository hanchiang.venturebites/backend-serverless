# Serverless - AWS Node.js Typescript

This project has been generated using the `aws-nodejs-typescript` template from the [Serverless framework](https://www.serverless.com/).

For detailed instructions, please refer to the [documentation](https://www.serverless.com/framework/docs/providers/aws/).

## Installation/deployment instructions

Depending on your preferred package manager, follow the instructions below to deploy your project.

> **Requirements**: NodeJS `lts/erbium (v.12.19.0)`. If you're using [nvm](https://github.com/nvm-sh/nvm), run `nvm use` to ensure you're using the same Node version in local and in your lambda's runtime.

### Using NPM

- Run `npm i` to install the project dependencies
- Run `npx sls deploy` to deploy this stack to AWS

### Using Yarn

- Run `yarn` to install the project dependencies
- Run `yarn sls deploy` to deploy this stack to AWS

## Test your service

This template contains a single lambda function triggered by an HTTP request made on the provisioned API Gateway REST API `/hello` route with `POST` method. The request body must be provided as `application/json`. The body structure is tested by API Gateway against `src/functions/hello/schema.ts` JSON-Schema definition: it must contain the `name` property.

- requesting any other path than `/hello` with any other method than `POST` will result in API Gateway returning a `403` HTTP error code
- sending a `POST` request to `/hello` with a payload **not** containing a string property named `name` will result in API Gateway returning a `400` HTTP error code
- sending a `POST` request to `/hello` with a payload containing a string property named `name` will result in API Gateway returning a `200` HTTP status code with a message saluting the provided name and the detailed event processed by the lambda

> :warning: As is, this template, once deployed, opens a **public** endpoint within your AWS account resources. Anybody with the URL can actively execute the API Gateway endpoint and the corresponding lambda. You should protect this endpoint with the authentication method of your choice.

### Locally

In order to test the hello function locally, run the following command:

- `npx sls invoke local -f hello --path src/functions/hello/mock.json` if you're using NPM
- `yarn sls invoke local -f hello --path src/functions/hello/mock.json` if you're using Yarn

Check the [sls invoke local command documentation](https://www.serverless.com/framework/docs/providers/aws/cli-reference/invoke-local/) for more information.

### Remotely

Copy and replace your `url` - found in Serverless `deploy` command output - and `name` parameter in the following `curl` command in your terminal or in Postman to test your newly deployed application.

```
curl --location --request POST 'https://myApiEndpoint/dev/hello' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Frederic"
}'
```

## Template features

### 3rd party librairies

- [json-schema-to-ts](https://github.com/ThomasAribart/json-schema-to-ts) - uses JSON-Schema definitions used by API Gateway for HTTP request validation to statically generate TypeScript types in your lambda's handler code base
- [middy](https://github.com/middyjs/middy) - middleware engine for Node.Js lambda. This template uses [http-json-body-parser](https://github.com/middyjs/middy/tree/master/packages/http-json-body-parser) to convert API Gateway `event.body` property, originally passed as a stringified JSON, to its corresponding parsed object
- [@serverless/typescript](https://github.com/serverless/typescript) - provides up-to-date TypeScript definitions for your `serverless.ts` service file

## Project overview
* Scrape events from sources(e27, techinasia, etc), upload to local and production database
* API server to serve requests
* API monitoring and status page with uptimerobot 

## Project structure

* scripts: Scripts to retrieve data from various sources, e.g. e27
  * `jobs/`: Background job that uses `scripts/` and `mongo/` to retrieve and save data to local and production database 
  * `scrape/`: Scrape for data(e.g. events) from sources, save data to local database
  * `mongo/`: Adhoc scripts to perform some actions on database
  * `e27/`, `techInAsia/`: Code to scrape data from those sources
* src
  * `controllers/`:  Lambda event handlers
  * `init/`: Initialisation code, e.g. database
  * `libs/`: Lambda shared code
  * `models/`: Database models
  * `services/`: Lambda logic
  * `types/`: Typescript types
  * `utils/`: Utility functions

* serverless.ts: Serverless service file
## Headers
Headers used for easy authentication/authorisation.
* app-name: `venturebites`
  * API authentication
* app-role: `admin`
  * For bulk retrieval of events

## Manual processes
* Cron job on localhost to run `scripts/jobs/index.sh` to scrape events data, save to local database, ingest to production database, dump local database.
* Aggregate unique event fields(`scripts/mongo/events/aggregateFields`), copy to frontend to be used as event search filters

**Install tools for running scripts**
* ts-node: `npm install -g ts-node`
* mongodump and mongoexport: https://docs.mongodb.com/database-tools/installation/installation/


## Architecture
![image](system-architecture.png)

## TODO
* Tests
* Learn to use serverless lambda middleware
* Proper authentication/authentication
* Better code organisation
* Aggregate event fields: Can consider writing an API to return such data, which frontend will call only once per app load
  * Updating database should trigger frontend deploy
* CICD
