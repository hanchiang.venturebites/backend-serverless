// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
// Moscow, St. Petersburg, Volgograd -> Europe/Moscow or Europe/Volgograd

import {
  splitDirtyTimezoneIntoCities,
  parseDate,
  convertOffsetStringToInt,
} from '../../../scripts/utils/e27';
import { expect } from 'chai';

/**
 * (GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London
 * (GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi
 * (GMT-10:00) Hawaii
 * (GMT-08:00) Pacific Time (US & Canada)
 * @param tzString
 */
describe('Get city, country name from e27 dirty timezone string', () => {
  it('Can get city/country for (GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', () => {
    const dirty =
      '(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London';
    const expected = [
      'Greenwich Mean Time : Dublin',
      'Edinburgh',
      'Lisbon',
      'London',
    ];
    expect(splitDirtyTimezoneIntoCities(dirty)).eql(expected);
  });

  it('Can get city/country for (GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi', () => {
    const dirty = '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi';
    const expected = ['Chennai', 'Kolkata', 'Mumbai', 'New Delhi'];
    expect(splitDirtyTimezoneIntoCities(dirty)).eql(expected);
  });

  it('Can get city/country for (GMT-10:00) Hawaii', () => {
    const dirty = '(GMT-10:00) Hawaii';
    const expected = ['Hawaii'];
    expect(splitDirtyTimezoneIntoCities(dirty)).eql(expected);
  });

  it('Can get city/country for (GMT-08:00) Pacific Time (US & Canada)', () => {
    const dirty = '(GMT-08:00) Pacific Time (US & Canada)';
    const expected = ['Pacific Time'];
    expect(splitDirtyTimezoneIntoCities(dirty)).eql(expected);
  });
});

describe('parseDate', () => {
  it('Can parse 2021-01-30 10:30:50 +05:00 correctly', () => {
    const parsed = parseDate('2021-01-30 10:30:50', '+05:00');
    expect(parsed.toISOString()).equal('2021-01-30T05:30:50.000Z');
  });

  it('Can parse 2021-01-30 10:30:50 -11:30 correctly', () => {
    const parsed = parseDate('2021-01-30 10:30:50', '-11:30');
    expect(parsed.toISOString()).equal('2021-01-30T22:00:50.000Z');
  });

  // utc is next day
  it('Can parse 2021-01-30 13:30:50 -11:30 correctly', () => {
    const parsed = parseDate('2021-01-30 13:30:50', '-11:30');
    expect(parsed.toISOString()).equal('2021-01-31T01:00:50.000Z');
  });

  it('Can parse 2021-01-30 10:30:50 +10:30 correctly', () => {
    const parsed = parseDate('2021-01-30 10:30:50', '+10:30');
    expect(parsed.toISOString()).equal('2021-01-30T00:00:50.000Z');
  });

  // utc is previous day
  it('Can parse 2021-01-30 09:30:50 +10:30 correctly', () => {
    const parsed = parseDate('2021-01-30 09:30:50', '+10:30');
    expect(parsed.toISOString()).equal('2021-01-29T23:00:50.000Z');
  });

  it('Can parse 2021-01-30 10:30:50 +00:00 correctly', () => {
    const parsed = parseDate('2021-01-30 10:30:50', '+00:00');
    expect(parsed.toISOString()).equal('2021-01-30T10:30:50.000Z');
  });

  it('Can parse 2021-01-30 10:30:50 -00:00 correctly', () => {
    const parsed = parseDate('2021-01-30 10:30:50', '-00:00');
    expect(parsed.toISOString()).equal('2021-01-30T10:30:50.000Z');
  });
});

describe('Convert string offset to int', () => {
  it('Can convert -11:30 to -690', () => {
    expect(convertOffsetStringToInt('-11:30')).equal(-690);
  });

  it('Can convert +00:00 to 0', () => {
    expect(convertOffsetStringToInt('+00:00')).equal(0);
  });

  it('Can convert -00:00 to 0', () => {
    expect(convertOffsetStringToInt('-00:00')).equal(0);
  });

  it('Can convert +11:30 to 690', () => {
    expect(convertOffsetStringToInt('+11:30')).equal(690);
  });
});
