import type { AWS } from '@serverless/typescript';

import { searchEvents, getEventDetail, healthCheck } from './src/controllers';

// https://www.serverless.com/framework/docs/providers/aws/guide/serverless.yml/
const serverlessConfiguration: AWS = {
  service: 'backend',
  app: 'venturebites',
  frameworkVersion: '2',
  custom: {
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: true,
    },
    /**
     * Configures throttling settings for the API Gateway stage
     * They apply to all http endpoints, unless specifically overridden
     */
    apiGatewayThrottling: {
      maxRequestsPerSecond: 100,
      maxConcurrentRequests: 25,
    },
    customDomain: {
      domainName: 'api.venturebites.co',
      basePath: '',
      certificateName: '*.venturebites.co',
      certificateArn:
        'arn:aws:acm:ap-southeast-1:516465004641:certificate/6b070866-3ccf-470b-be98-4558561feafd',
      endpointType: 'regional',
      createRoute53Record: true,
      securityPolicy: 'tls_1_2',
      apiType: 'rest',
      stage: '${env:STAGE}',
    },
  },
  plugins: [
    'serverless-webpack',
    'serverless-dotenv-plugin',
    'serverless-offline',
    'serverless-api-gateway-throttling',
    'serverless-domain-manager',
  ],
  provider: {
    name: 'aws',
    runtime: 'nodejs12.x',
    versionFunctions: false,
    stage: '${env:STAGE}',
    profile: '${env:AWS_PROFILE}',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    region: 'ap-southeast-1',
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_ENV: '${env:NODE_ENV}',
      APP_ENV: '${env:APP_ENV}',
      MONGO_CONNECTION_URI: '${env:MONGO_CONNECTION_URI}',
      SLACK_TOKEN: '${env:SLACK_TOKEN}',
    },
    endpointType: 'regional',
    lambdaHashingVersion: '20201221',
    memorySize: 256,
    timeout: 10,
  },
  functions: { searchEvents, getEventDetail, healthCheck },
  useDotenv: true,
};

module.exports = serverlessConfiguration;
